const _ = require('lodash')
module.exports = function($elements, interval){
    if(!($elements instanceof jQuery) || !$elements.length){
        throw new Error('Invalid $elements')
    }

    var delay = 0;
    $elements.each(function(){
        if($(this).hasClass('group')){
            delay = 0;
            module.exports($(this).children(), interval);
            return true;
        }

        if($(this).hasClass('no-animation') || $(this).hasClass('navigation-buttons')){
            return true;
        }

        var classes = $(this).attr('data-ani-class');
        if(_.isString(classes) && classes.length){
            var animationClasses = classes;
        }
        else{
            animationClasses = 'fadeInUp';
        }

        var _delay = $(this).attr('data-ani-delay')
        if(!_.isString(_delay) || !_delay.length){
            _delay = delay;
        }

        $(this).attr({
            'data-ani-class': animationClasses,
            'data-ani-delay': _delay
        });

        delay+= _.isNumber(interval) ? interval : 250;
    })
}